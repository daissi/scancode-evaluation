Files: *
Copyright: 1998-2023, The OpenSSL Project
 1995-1998, Eric A. Young, Tim J. Hudson
License: Apache-2.0

Files: CONTRIBUTING.md
Copyright: 20xx-20yy The OpenSSL Project Authors.
License: Apache-2.0

Files: Configurations/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: Configure
 config
 e_os.h
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: VMS/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/cmp.c
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: apps/ecparam.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: apps/include/cmp_mock_srv.h
Copyright: 2018-2023, The OpenSSL Project Authors.
 2018-2020, Siemens AG
License: Apache-2.0

Files: apps/include/vms_term_sock.h
Copyright: 2016, VMS Software, Inc.
 2016, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/lib/cmp_mock_srv.c
Copyright: 2018-2023, The OpenSSL Project Authors.
 2018-2020, Siemens AG
License: Apache-2.0

Files: apps/lib/tlssrp_depr.c
Copyright: 2005, Nokia.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/lib/vms_term_sock.c
Copyright: 2016-2022, The OpenSSL Project Authors.
 2016, VMS Software, Inc.
License: Apache-2.0

Files: apps/rehash.c
Copyright: 2015-2023, The OpenSSL Project Authors.
 2013, 2014, Timo TerÃ¤s <timo.teras@gmail.com>
License: Apache-2.0

Files: apps/s_client.c
Copyright: 2005, Nokia.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/s_server.c
Copyright: 2005, Nokia.
 2002, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/speed.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: apps/srp.c
Copyright: 2004-2023, The OpenSSL Project Authors.
 2004, EdelKey Project.
License: Apache-2.0

Files: apps/tsget.in
Copyright: 2002-2021, The OpenSSL Project Authors.
 2002, The OpenTSA Project.
License: Apache-2.0

Files: crypto/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/LPdir_nyi.c
 crypto/LPdir_unix.c
 crypto/LPdir_vms.c
 crypto/LPdir_win.c
 crypto/LPdir_win32.c
 crypto/LPdir_wince.c
Copyright: 2004-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-2-clause

Files: crypto/aes/aes_core.c
 crypto/aes/aes_x86core.c
Copyright: 2002-2022, The OpenSSL Project Authors.
License: Apache-2.0 and/or public-domain

Files: crypto/aes/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/aes/asm/aes-ia64.S
 crypto/aes/asm/aes-sparcv9.pl
 crypto/aes/asm/bsaes-x86_64.pl
 crypto/aes/asm/vpaes-armv8.pl
 crypto/aes/asm/vpaes-ppc.pl
 crypto/aes/asm/vpaes-x86.pl
 crypto/aes/asm/vpaes-x86_64.pl
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/aes/asm/aest4-sparcv9.pl
Copyright: 2004-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-2-clause

Files: crypto/aria/*
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/bn/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/bn/asm/bn-586.pl
 crypto/bn/asm/co-586.pl
 crypto/bn/asm/ia64.S
 crypto/bn/asm/mips.pl
 crypto/bn/asm/ppc.pl
 crypto/bn/asm/s390x.S
 crypto/bn/asm/sparcv8.S
 crypto/bn/asm/sparcv8plus.S
 crypto/bn/asm/x86_64-gcc.c
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/bn/asm/rsaz-avx2.pl
 crypto/bn/asm/rsaz-avx512.pl
 crypto/bn/asm/rsaz-x86_64.pl
Copyright: 2013-2023, The OpenSSL Project Authors.
 2012, 2020, Intel Corporation.
License: Apache-2.0

Files: crypto/bn/asm/sparct4-mont.pl
Copyright: 2004-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-2-clause

Files: crypto/bn/bn_gf2m.c
 crypto/bn/bn_rsa_fips186_4.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/bn/rsaz_exp.c
 crypto/bn/rsaz_exp_x2.c
Copyright: 2013-2023, The OpenSSL Project Authors.
 2012, 2020, Intel Corporation.
License: Apache-2.0

Files: crypto/bn/rsaz_exp.h
Copyright: 2020, Intel Corporation.
 2013-2022, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/camellia/asm/*
Copyright: 2008-2020, The OpenSSL Project Authors.
License: Apache-2.0 and/or GPL-2+

Files: crypto/camellia/asm/cmllt4-sparcv9.pl
Copyright: 2004-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-2-clause

Files: crypto/chacha/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/cmp/*
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: crypto/cmp/cmp_err.c
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/crmf/*
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: crypto/crmf/crmf_err.c
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/cryptlib.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/des/asm/dest4-sparcv9.pl
Copyright: 2004-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-2-clause

Files: crypto/ec/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/ec/asm/ecp_nistp521-ppc64.pl
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/ec/asm/ecp_nistz256-x86_64.pl
Copyright: 2015, CloudFlare, Inc.
 2014-2022, The OpenSSL Project Authors.
 2014, Intel Corporation.
License: Apache-2.0

Files: crypto/ec/curve448/*
Copyright: 2017-2022, The OpenSSL Project Authors.
 2014-2016, Cryptography Research, Inc.
License: Apache-2.0

Files: crypto/ec/curve448/curve448_local.h
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/ec/ec2_oct.c
 crypto/ec/ec2_smpl.c
 crypto/ec/ec_curve.c
 crypto/ec/ec_key.c
 crypto/ec/ec_oct.c
 crypto/ec/ecdh_ossl.c
 crypto/ec/eck_prn.c
 crypto/ec/ecp_oct.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/ec/ec_cvt.c
 crypto/ec/ec_lib.c
 crypto/ec/ec_local.h
 crypto/ec/ec_mult.c
 crypto/ec/ecp_mont.c
 crypto/ec/ecp_nist.c
 crypto/ec/ecp_smpl.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/ec/ecp_nistz256.c
Copyright: 2015, CloudFlare, Inc.
 2014-2022, The OpenSSL Project Authors.
 2014, Intel Corporation.
License: Apache-2.0

Files: crypto/engine/eng_fat.c
 crypto/engine/eng_list.c
 crypto/engine/eng_local.h
 crypto/engine/eng_openssl.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/evp/e_aria.c
 crypto/evp/kdf_lib.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/evp/e_sm4.c
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: crypto/http/http_client.c
Copyright: 2015-2022, Siemens AG
 2000-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/md5/asm/md5-sparcv9.pl
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/mem_sec.c
Copyright: 2015-2023, The OpenSSL Project Authors.
 2004-2014, Akamai Technologies.
License: Apache-2.0

Files: crypto/modes/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/param_build.c
 crypto/params.c
 crypto/params_from_text.c
 crypto/sparse_array.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/poly1305/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/property/*
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/property/property_err.c
 crypto/property/property_query.c
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/rc4/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/rsa/rsa_mp.c
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, BaishanCloud.
License: Apache-2.0

Files: crypto/rsa/rsa_sp800_56b_check.c
 crypto/rsa/rsa_sp800_56b_gen.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: crypto/sha/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: crypto/sha/asm/sha512-x86_64.pl
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: crypto/sm2/sm2_crypt.c
 crypto/sm2/sm2_sign.c
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: crypto/sm3/*
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: crypto/sm4/*
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: crypto/srp/*
Copyright: 2004-2023, The OpenSSL Project Authors.
 2004, EdelKey Project.
License: Apache-2.0

Files: crypto/x509/v3_pci.c
 crypto/x509/v3_pcia.c
Copyright: 2004-2021, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-3-clause

Files: debian/*
Copyright: 1998-2023, The OpenSSL Project
License: Apache-2.0 and/or OpenSSL

Files: debian/rules
Copyright: Designs and Patents Act 1988.)
 1994, 1995, Ian Jackson.
License: UNKNOWN

Files: debian/tests/*
Copyright: 2015-2018, The OpenSSL Project Authors.
License: OpenSSL

Files: demos/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: doc/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: doc/internal/man3/OPENSSL_SA.pod
 doc/internal/man3/OSSL_METHOD_STORE.pod
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: doc/man3/BIO_f_ssl.pod
Copyright: no-info-found
License: Apache-2.0

Files: doc/man3/DEFINE_STACK_OF.pod
Copyright: 2000-2022, The OpenSSL Project Authors.
License: UNKNOWN

Files: doc/man3/EVP_sm3.pod
 doc/man3/EVP_sm4_cbc.pod
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: doc/man7/EVP_KDF-KB.pod
Copyright: 2019-2022, The OpenSSL Project Authors.
 2019, Red Hat, Inc.
License: Apache-2.0

Files: doc/man7/EVP_KDF-SS.pod
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: doc/man7/EVP_RAND.pod
Copyright: 2000-2022, The OpenSSL Project Authors.
License: UNKNOWN

Files: engines/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: engines/asm/*
Copyright: 1998-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or OpenSSL

Files: external/*
Copyright: 2013, Mark Jason Dominus <mjd@cpan.org>.
License: Artistic-1.0 and/or GPL-1 and/or Perl or Perl

Files: external/perl/Text-Template-1.56/lib/*
Copyright: 2013, M. J. Dominus.
License: UNKNOWN

Files: external/perl/Text-Template-1.56/lib/Text/Template/*
Copyright: 2013, Mark Jason Dominus <mjd@cpan.org>.
License: UNKNOWN

Files: fuzz/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: include/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: include/crypto/aria.h
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: include/crypto/sha.h
 include/crypto/sparse_array.h
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: include/crypto/sm2.h
 include/crypto/sm4.h
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: include/internal/o_dir.h
Copyright: 2004-2023, The OpenSSL Project Authors.
License: Apache-2.0 and/or BSD-2-clause

Files: include/internal/property.h
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: include/internal/sm3.h
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: include/openssl/bn.h
 include/openssl/crypto.h.in
 include/openssl/engine.h
 include/openssl/ssl3.h
 include/openssl/x509.h.in
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: include/openssl/cmp.h.in
 include/openssl/cmp_util.h
 include/openssl/crmf.h.in
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: include/openssl/ec.h
 include/openssl/param_build.h
 include/openssl/params.h
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: include/openssl/http.h
Copyright: 2015-2022, Siemens AG
 2000-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: include/openssl/srp.h.in
Copyright: 2004-2023, The OpenSSL Project Authors.
 2004, EdelKey Project.
License: Apache-2.0

Files: include/openssl/ssl.h.in
 include/openssl/tls1.h
Copyright: 2005, Nokia.
 2002, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ms/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: os-dep/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: providers/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: providers/implementations/kdfs/kbkdf.c
Copyright: 2019-2022, The OpenSSL Project Authors.
 2019, Red Hat, Inc.
License: Apache-2.0

Files: providers/implementations/kdfs/sskdf.c
 providers/implementations/kdfs/x942kdf.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: providers/implementations/rands/crngt.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: ssl/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ssl/s3_enc.c
 ssl/ssl_asn1.c
 ssl/ssl_sess.c
 ssl/ssl_stat.c
 ssl/ssl_txt.c
 ssl/t1_enc.c
Copyright: 2005, Nokia.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ssl/s3_lib.c
 ssl/ssl_ciph.c
 ssl/ssl_lib.c
 ssl/ssl_local.h
Copyright: 2005, Nokia.
 2002, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ssl/ssl_cert.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ssl/statem/statem_clnt.c
 ssl/statem/statem_srvr.c
Copyright: 2005, Nokia.
 2002, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ssl/statem/statem_lib.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: ssl/tls_srp.c
Copyright: 2004-2023, The OpenSSL Project Authors.
 2004, EdelKey Project.
License: Apache-2.0

Files: test/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/certs/*
Copyright: 2016-2023, The OpenSSL Project Authors.
 2016, Viktor Dukhovni <openssl-users@dukhovni.org>.
License: Apache-2.0

Files: test/ciphername_test.c
 test/rsa_mp_test.c
 test/servername_test.c
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, BaishanCloud.
License: Apache-2.0

Files: test/cmp_asn_test.c
 test/cmp_client_test.c
 test/cmp_ctx_test.c
 test/cmp_hdr_test.c
 test/cmp_msg_test.c
 test/cmp_protect_test.c
 test/cmp_server_test.c
 test/cmp_status_test.c
 test/cmp_vfy_test.c
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: test/ecdsatest.c
 test/evp_kdf_test.c
 test/ffc_internal_test.c
 test/lhash_test.c
 test/param_build_test.c
 test/params_api_test.c
 test/params_conversion_test.c
 test/property_test.c
 test/sparse_array_test.c
 test/stack_test.c
 test/test_test.c
 test/x509_dup_cert_test.c
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: test/ectest.c
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/helpers/cmp_testlib.c
 test/helpers/cmp_testlib.h
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: test/http_test.c
Copyright: 2015-2022, Siemens AG
 2000-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/recipes/01-test_test.t
 test/recipes/02-test_lhash.t
 test/recipes/02-test_localetest.t
 test/recipes/02-test_sparse_array.t
 test/recipes/02-test_stack.t
 test/recipes/03-test_params_api.t
 test/recipes/20-test_enc_more.t
 test/recipes/30-test_evp_kdf.t
 test/recipes/60-test_x509_dup_cert.t
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: test/recipes/02-test_internal_ctype.t
Copyright: 2002, 2017, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/recipes/03-test_internal_sm3.t
Copyright: 2021, [UnionTech](https://www.uniontech.com).
 2021, 2022, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/recipes/03-test_internal_sm4.t
Copyright: 2017, [Ribose Inc.](https://www.ribose.com).
 2017, 2018, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/recipes/15-test_mp_rsa.t
 test/recipes/80-test_ciphername.t
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, BaishanCloud.
License: Apache-2.0

Files: test/recipes/65-test_cmp_asn.t
 test/recipes/65-test_cmp_client.t
 test/recipes/65-test_cmp_ctx.t
 test/recipes/65-test_cmp_hdr.t
 test/recipes/65-test_cmp_msg.t
 test/recipes/65-test_cmp_protect.t
 test/recipes/65-test_cmp_server.t
 test/recipes/65-test_cmp_status.t
 test/recipes/65-test_cmp_vfy.t
 test/recipes/80-test_cmp_http.t
 test/recipes/81-test_cmp_cli.t
Copyright: 2015-2020, Siemens AG
 2007-2023, The OpenSSL Project Authors.
 2007-2020, Nokia
License: Apache-2.0

Files: test/recipes/70-test_servername.t
Copyright: 2017, BaishanCloud.
 2017, 2018, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/recipes/95-test_external_pyca_data/*
Copyright: 2002-2023, The OpenSSL Project Authors.
 2002, 2017-2020, Oracle and/or its affiliates.
License: Apache-2.0

Files: test/sm3_internal_test.c
Copyright: 2021, UnionTech.
 2021, 2022, The OpenSSL Project Authors.
License: Apache-2.0

Files: test/sm4_internal_test.c
Copyright: 2017-2023, The OpenSSL Project Authors.
 2017, Ribose Inc.
License: Apache-2.0

Files: test/ssl_old_test.c
Copyright: 2005, Nokia.
 2002, Oracle and/or its affiliates.
 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: tools/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: util/*
Copyright: 1995-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: util/check-format-test-negatives.c
 util/check-format-test-positives.c
Copyright: 2015-2022, Siemens AG
 2000-2023, The OpenSSL Project Authors.
License: Apache-2.0

Files: util/check-format.pl
Copyright: 2020-2023, The OpenSSL Project Authors.
 2019-2022, Siemens AG
License: Apache-2.0 and/or OpenSSL

