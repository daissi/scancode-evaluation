#!/usr/bin/env python3

import argparse
import yaml

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--yaml",
        required=True,
        type=argparse.FileType("r"),
        help="input file in YAML format",
    )
    args = parser.parse_args()

    data = yaml.load(args.yaml, Loader=yaml.CSafeLoader)
    data_headers = data["headers"][0]
    data_files = data["files"]

    file_count = 0
    file_license_count = 0
    dir_count = 0
    dir_license_count = 0

    for file in data_files:
        if file["type"] == "file":
            file_count = file_count+1
            #print(f'DEBUG: FILE: {file["path"]}')
            if file["detected_license_expression"] is not None:
                file_license_count = file_license_count+1
                #print(f'DEBUG:    LIC: {file["detected_license_expression"]}')
        elif file["type"] == "directory":
            dir_count = dir_count+1
            #print(f'DEBUG: DIR: {file["path"]}')
            if file["detected_license_expression"] is not None:
                dir_license_count = dir_license_count+1
                #print(f'DEBUG:                   LIC: {file["detected_license_expression"]}')
        else:
            print(f'DEBUG: ??? {file}')

    print(f'\nscancode stat: len(data_files): {len(data_files)}')
    print(f'scancode stat: files_count: {data_headers["extra_data"]["files_count"]}\n')

    print(f'file_count: {file_count}')
    print(f'file_license_count: {file_license_count}')
    print(f'file_license_detected %: {100*file_license_count/file_count}\n')
    print(f'dir_count: {dir_count}')
    print(f'dir_license_count: {dir_license_count}')
    print(f'dir_license_detected %: {100*dir_license_count/dir_count}')
